import os
from os import listdir
import re

print("Enter path\n")
path = input()  
regex = r'\d+'  # задание регулярного выражения
count = 0  # счетчик для генерации имен выходных файлов
result = []  # список для хранения путей к файлам директории
isFile = os.path.isfile(path)  # проверка что путь до файла
isDirectory = os.path.isdir(path)  # проверка что путь до директории

if isFile:  # если путь до файла
    file = open(path, "r")  # открыть файл для чтения
    out = open("result.txt", "w")  # открыть файл для записи результат
    for line in file.readlines():  # читать файл построчно
        if re.findall(regex, line):  # если совпадает с регулярным выражением, записать в выходной файл
            out.write(line)
        print(line)  # печать строки в консоль
    file.close()
    out.close()
    
elif isDirectory:  # если путь до директории
    files = [f for f in listdir(path) if f.endswith(".txt")]  # список всех файлов с расширением .txt
    for elem in files:
        res_file = str(path) + r"\\" + "result" + str(count) + ".txt"  # Имя выходного файла
        print(res_file)
        out = open(res_file, "w")  # открыть очередной файл для записи
        file = open(path + r"\\" + str(elem), "r")  # открыть очередной файл для чтения
        for line in file.readlines():
            if re.findall(regex, line):  # если совпадает с регулярным выражением, записать в выходной файл
                out.write(line)
            print(line)  # печать в консоль
        print('\n' + f'End of {str(elem)}' + '\n')  # разделение вывод в консоль по файлам
        file.close()
        count += 1
